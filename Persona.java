
public class Persona {
	//Declaramos variables privadas de la clase
    private String nombre, aPaterno, aMaterno;
    protected int edad;
	static int num;

	
	//Constructor vacio
    public Persona(){
		num++;
    }
	//Constructor que recibe nombre
    public Persona(String nombre){
    	this.nombre=nombre;
		num ++;
    }
	//Constructor que recibe nombre y edad
    public Persona(String nombre, int edad){
    	this(nombre);   //no se suma poruqe llama al constructor nombre donde ya suma
    	this.edad=edad;
    }
    public Persona(String nombre,int edad, String aMaterno, String aPaterno){
        this.nombre=nombre;
        this.aPaterno=aPaterno;
        this.aMaterno=aMaterno;
        this.edad=edad;
		num++;
    }
    public Persona(String nombre,String aPaterno, String aMaterno, int edad){
        this(nombre,edad);
        this.aPaterno=aPaterno;
        this.aMaterno=aMaterno;
    }
	//Métodos para asignar o actualizar el contenido de las 
	//variables de la clase
    public void setNombre(String nombre){
    	this.nombre=nombre;
    }
    public void setPaterno(String aPaterno){
    	this.aPaterno=aPaterno;
    }
    public void setMaterno(String aMaterno){
    	this.aMaterno=aMaterno;
    }
    public void setEdad(int edad){
    	this.edad=edad;
    }
	//Metodos para regresar los valores de cada variable
    public String getNombre(){
    	return nombre;
    }
    public String getPaterno(){
    	return aPaterno;
    }
    public String getMaterno(){
    	return aMaterno;
    }
    public int getEdad(){
    	return edad;
    }
	//Método para imprimir por pantalla
	//la información de la persona
	//(Puede variar dependiendo de cada quien)
    public void imprime(){
        if (nombre==null){
            System.out.println("La persona no tiene nombre!");
        }
        else{
            if (aPaterno==null||aMaterno==null)
                System.out.println("Soy ["+nombre+"] pero No tengo apellidos!");
            else
                if (edad>=0)
                    System.out.println("Me llamo "+nombre+" "+aPaterno+" "+aMaterno+"\nTengo ["+edad+"] años de edad");
                else
                    System.out.println("No puedo tener edad negativa!");
        }
    }
}