public class Libro {
    //Declaraciones de variables
    private String titulo,autor,editorial;
    private int pag,anioimpresion,aniodistribucion,isbn;
	static int numero;
    
     //Constructor 
    public Libro(String titulo,String autor)  //te permite hacer un objeto que no modifica su valor
	{
	   this.titulo;
       this.autor;	   
	   numero++;
	}
	 
	 public Libro()
	{
	   numero++;
    }
	 
    //Secciones de set
    public void setTitulo(String titulo){
        this.titulo=titulo;
    }
    public void setAutor(String autor){
        this.autor=autor;
    }
    public void setEditorial(String editorial){
        this.editorial=editorial;
    }
    public void setPag(int pag){
        this.pag=pag;
    }

    public void setAnioimpresion(int anioimpresion){
        this.anioimpresion=anioimpresion;
    }
	 public void setAniodistribucion(int aniodistribucion){
        this.aniodistribucion=aniodistribucion;
    }
    public void setIsbn(int isbn){
        this.isbn=isbn;
    }
	public void setNumero(int numero){
        this.numero=numero;
    }
    //Definiciones de get
    public String getTitulo(){
        return titulo;
    }
	public String getAutor(){
        return autor;
    }
	public String getEditorial(){
        return editorial;
    }
	public int getPag(){
        return pag;
    }
	public int getAnioimpresion(){
        return anioimpresion;
    }
	public int getAniodistribucion(){
        return aniodistribucion;
    }
	
	public int getIsbn(){
        return isbn;
    }
	public int getNumero(){
        return numero;
    }
	
	 //Metodos diferentes a set y get
    public void imprime(){
        
        System.out.println("Nombre del libro: "+titulo+" Autor: "+ autor+ "  editorial: "+editorial + "\n");
    }
	
	
}