public class Usuario extends Persona {
 //Declaraciones de variables
    private String clave,calle,col,ciudad;
    private int id;
	   
   
    //Secciones de set
    public void setClave(String clave){
        this.clave=clave;
    }
	
	public void setCalle(String calle){
        this.calle=calle;
    }

    public void setCol(String col){
        this.col=col;
    }

    public void setCiudad(String ciudad){
        this.ciudad=ciudad;
    }
	

    public void setId(int id){
        this.id=id;
    }
	
	
	 //Definiciones de get
    public String getClave(){
        return clave;
    }
    public String getCalle(){
        return calle;
    }
	 public String getCol(){
        return col;
    }
	 public String getCiudad(){
        return ciudad;
    }
    public int getId(){
        return id;
    }
	
		 //Metodos diferentes a set y get
    public void imprime(){
        
        System.out.println("Yo vivia en la calle : "+calle+" de la colonia: "+ col+ "  en la ciudad: "+ciudad + "\n");
    }
	
	
}